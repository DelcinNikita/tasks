package com.example.task5;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Task {

    public static void main(String args[]) {
        System.out.println(encrypt("Hello"));
        byte[] arr1 = {72, 33, -73, 84, -12, -3, 13, -13, -68};
        System.out.println(decrypt(arr1));
        System.out.println(encrypt("Sunshine"));
        System.out.println(canMove("Rook", "A8", "H8"));
        System.out.println(canMove("Bishop", "A7", "G1"));
        System.out.println(canMove("Queen", "C4", "D6"));
        /*System.out.println(canMove("King", "C4", "D6"));
        System.out.println(canMove("King", "C4", "C5"));
        System.out.println(canMove("Pawns", "C4", "C6"));
        System.out.println(canMove("Pawns", "C4", "C5"));
        System.out.println(canMove("Knight", "C4", "E3"));
        System.out.println(canMove("Knight", "C4", "E4"));*/
        System.out.println(canComplete("butl", "beautiful"));
        System.out.println(canComplete("butlz", "beautiful"));
        System.out.println(canComplete("tulb", "beautiful"));
        System.out.println(canComplete("bbutl", "beautiful"));
        System.out.println(sumDigProd(16, 28));
        System.out.println(sumDigProd(0));
        System.out.println(sumDigProd(1, 2, 3, 4, 5, 6));
        List<String> arr2 = Arrays.asList("toe", "ocelot", "maniac");
        List<String> arr3 = Arrays.asList("many", "carriage", "emit", "apricot", "animal");
        List<String> arr4 = Arrays.asList("hoops", "chuff", "bot", "bottom");
        System.out.println(sameVowelGroup(arr2));
        System.out.println(sameVowelGroup(arr3));
        System.out.println(sameVowelGroup(arr4));
        System.out.println(validateCard(1234567890123456L));
        System.out.println(validateCard(1234567890123452L));
        System.out.println(numToEng(0));
        System.out.println(numToEng(18));
        System.out.println(numToEng(126));
        System.out.println(numToEng(909));
        System.out.println(numToRu(18));
        System.out.println(numToRu(126));
        System.out.println(numToRu(909));
        System.out.println(numToRu(519));
        System.out.println(getSha256Hash("password123"));
        System.out.println(getSha256Hash("Fluffy@home"));
        System.out.println(getSha256Hash("Hey dude!"));
        System.out.println(correctTitle("jOn SnoW, kINg IN thE noRth."));
        System.out.println(correctTitle("sansa stark, lady of winterfell."));
        System.out.println(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN."));
        System.out.println(hexLattice(1));
        System.out.println(hexLattice(7));
        System.out.println(hexLattice(19));
        System.out.println(hexLattice(21));
        /*System.out.println(hexLattice(37));*/

    }

    private static List<Byte> encrypt(String a) {
        byte[] a1 = a.getBytes(StandardCharsets.US_ASCII);
        List<Byte> res = new ArrayList<>();
        res.add(a1[0]);
        for (int i = 1; i < a1.length; i++) res.add((byte) (a1[i] - a1[i-1]));
        return res;
    }

    private static String decrypt(byte[] a) {
        String res = "";
        byte k = 0;
        for (int i = 0; i < a.length; i++) {
            k += a[i];
            res += (char) k;
        }
        return res;
    }

    private static boolean canMove(String a, String b, String c) {
        List<Character> fields = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');
        if (a.equals("Rook")) {
            if (b.charAt(1) == c.charAt(1)) return true;
            if (b.charAt(0) == c.charAt(0)) return true;
        }
        if (a.equals("Bishop")) {
            int counter = 1;
            int k1 = Integer.parseInt(String.valueOf(b.charAt(1))) - fields.indexOf(b.charAt(0));
            int k2 = fields.size() - fields.indexOf(b.charAt(0))-1;
            for (int i = 0; i < fields.size(); i++) {
                if ((fields.get(i) + String.valueOf(k1 + i)).equals(c)) return true;
            }
            for (int i = k2; i > 0; i--) {
                if ((fields.get(i-1) + String.valueOf(counter)).equals(c)) return true;
                counter += 1;
            }
        }
        if (a.equals("Queen")) {
            if (b.charAt(1) == c.charAt(1)) return true;
            if (b.charAt(0) == c.charAt(0)) return true;
            int counter = 1;
            int k1 = Integer.parseInt(String.valueOf(b.charAt(1))) - fields.indexOf(b.charAt(0));
            int k2 = fields.size() - fields.indexOf(b.charAt(0))-1;
            for (int i = 0; i < fields.size(); i++) {
                if ((fields.get(i) + String.valueOf(k1 + i)).equals(c)) return true;
            }
            for (int i = k2; i > 0; i--) {
                if ((fields.get(i-1) + String.valueOf(counter)).equals(c)) return true;
                counter += 1;
            }
        }
        if (a.equals("King")) {
            int i = Integer.parseInt(String.valueOf(c.charAt(1)));
            int i1 = Integer.parseInt(String.valueOf(b.charAt(1)));
            if ((c.charAt(0) == b.charAt(0) || (fields.indexOf(b.charAt(0))-1) > 0
                 && c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))-1)) ||
                 (fields.indexOf(b.charAt(0))+1) < 8 && c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))+1)
                 && (i == i1 || i == i1 + 1 || i == i1 - 1))
                return true;
        }
        if (a.equals("Pawns")) {
            return c.charAt(0) == b.charAt(0) && String.valueOf(c.charAt(1)).equals(String.valueOf(Character.getNumericValue(b.charAt(1)) + 1));
        }
        if (a.equals("Knight")) {
            if ((c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))-1) &&
               (String.valueOf(Character.getNumericValue(b.charAt(1)) + 2).equals(String.valueOf(c.charAt(1))) ||
               String.valueOf(Character.getNumericValue(b.charAt(1)) - 2).equals(String.valueOf(c.charAt(1))))) ||
               ((c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))+1) &&
               (String.valueOf(Character.getNumericValue(b.charAt(1)) + 2).equals(String.valueOf(c.charAt(1))) ||
               String.valueOf(Character.getNumericValue(b.charAt(1)) - 2).equals(String.valueOf(c.charAt(1)))))) ||
               ((c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))-2) &&
               (String.valueOf(Character.getNumericValue(b.charAt(1)) + 1).equals(String.valueOf(c.charAt(1))) ||
               String.valueOf(Character.getNumericValue(b.charAt(1)) - 1).equals(String.valueOf(c.charAt(1)))))) ||
               ((c.charAt(0) == fields.get(fields.indexOf(b.charAt(0))+2) &&
               (String.valueOf(Character.getNumericValue(b.charAt(1)) + 1).equals(String.valueOf(c.charAt(1))) ||
               String.valueOf(Character.getNumericValue(b.charAt(1)) - 1).equals(String.valueOf(c.charAt(1))))))) return true;
        }
        return false;
    }

    private static boolean canComplete(String a, String b) {
        boolean res = false;
        if (b.indexOf(a.charAt(0)) == -1) return false;
        for (int i = 1; i < a.length(); i++) {
            if (b.indexOf(a.charAt(i)) > b.indexOf(a.charAt(i-1)) && b.indexOf(a.charAt(i)) != -1) res = true;
            else return false;
        }
        return res;
    }

    private static int sumDigProd(int ... a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }

        String[] as = String.valueOf(sum).split("");
        String k = String.valueOf(sum);
        while (k.length() > 1) {
            int mul = 1;
            for (String s : as) mul *= Integer.parseInt(s);
            k = String.valueOf(mul);
            as = k.split("");
        }
        return Integer.parseInt(k);
    }

    private static List<String> sameVowelGroup(List<String> a) {
        List<Character> vokals = Arrays.asList('A', 'E', 'I', 'O', 'U', 'Y', 'a', 'e', 'i', 'o', 'u', 'y');
        List<Character> aVokals = new ArrayList<>();
        List<String> ans = new ArrayList<>();
        for (int i = 0; i < a.get(0).length(); i++) {
            if (vokals.contains(a.get(0).charAt(i))) {
                aVokals.add(a.get(0).charAt(i));
            }
        }
        ans.add(a.get(0));


        for (int i = 1; i < a.size(); i++) {
            boolean wordContains = true;
            for (int j = 0; j < aVokals.size(); j++) {
                if (!a.get(i).contains(aVokals.get(j).toString())) {
                    wordContains = false;
                    break;
                }
            }
            if (wordContains) ans.add(a.get(i));
        }
        return ans;
    }

    private static boolean validateCard(long a) {
        List<Integer> as = Arrays.stream(String.valueOf(a).split(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        int checkDigit = as.get(as.size()-1);
        as.remove(as.size()-1);
        Collections.reverse(as);
        for (int i = 0; i < as.size(); i += 2) {
            if (as.get(i) * 2 < 10) as.set(i, as.get(i) * 2);
            else as.set(i, Character.getNumericValue(String.valueOf(as.get(i)*2).charAt(0)) +
                    Character.getNumericValue(String.valueOf(as.get(i)*2).charAt(1)));
        }
        int sum = 0;
        for (int i = 0; i < as.size(); i++) sum += as.get(i);
        String sums = String.valueOf(sum);
        return checkDigit == 10 - Character.getNumericValue(sums.charAt(sums.length()-1));
    }

    private static String numToEng(int a) {
        String ans = "";
        List<String> units = Arrays.asList("", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen");
        List<String> tens = Arrays.asList("", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety");
        List<String> hundredths = Arrays.asList("", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred");
        if (a == 0) return "zero";
        else if (a < 20) return units.get(a);
        else {
            int k = a;
            int len = String.valueOf(a).length();
            List<String> s = new ArrayList<>();
            while (len > 0) {
                if (len == 3) s.add(units.get(k % 10));
                if (len == 2) s.add(tens.get(k % 10));
                if (len == 1) s.add(hundredths.get(k % 10));
                k /= 10;
                len -= 1;
            }
            for (int i = s.size()-1; i >= 0; i--) {
                if (!s.get(i).equals("")) ans += s.get(i) + " ";
            }
        }
        return ans;
    }

    private static String numToRu(int a) {
        String ans = "";
        List<String> units = Arrays.asList("", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать");
        List<String> tens = Arrays.asList("", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто");
        List<String> hundredths = Arrays.asList("", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот");
        if (a == 0) return "ноль";
        else if (a < 20) return units.get(a);
        else {
            int k = a;
            int len = String.valueOf(a).length();
            List<String> s = new ArrayList<>();
            while (len > 0) {
                if (len == 3) {
                    if (k % 100 > 19) s.add(units.get(k % 10));
                    else {
                        s.add(units.get(k % 100));
                        k /= 10;
                        len -= 1;
                    }
                }
                if (len == 2) s.add(tens.get(k % 10));
                if (len == 1) s.add(hundredths.get(k % 10));
                k /= 10;
                len -= 1;
            }
            for (int i = s.size()-1; i >= 0; i--) {
                if (!s.get(i).equals("")) ans += s.get(i) + " ";
            }
        }
        return ans;
    }

    private static String getSha256Hash(String a) {
        return org.apache.commons.codec.digest.DigestUtils.sha256Hex(a);
    }

    private static String correctTitle(String a) {
        String[] as = a.split(" ");
        String s = "";
        List<String> dots = Arrays.asList(".", ",", ":", ";", "!", "?");
        List<String> extras = Arrays.asList("in", "the", "of", "and");
        for (String word : as) {
            if (word.equals("-")) s += word + " ";
            String word2 = word.toLowerCase();
            String dot2 = "";
            for (String dot : dots) if (word2.contains(dot)) {
                word2 = word2.replace(dot, "");
                dot2 += dot;
            }
                if (extras.contains(word2)) s += word2 + dot2 + " ";
                else {
                    String[] chars = word2.split("");
                    s += chars[0].toUpperCase();
                    for (int i = 1; i < chars.length; i++) s += chars[i];
                    s += dot2 + " ";
                }
        }
        return s;
    }

    private static String hexLattice(int a) {
        if (a == 1) return " o ";
        String s = "";
        int num = 1;
        int k = 0;
        while (num < a) {
            k += 1;
            num += 6 * k;
        }
        if (num != a) return "Invalid";
        else {
            int k1 = k+1;
            int strokeK;
            if (k1 % 2 == 0) strokeK = k1 * 2 + 2;
            else strokeK = k1 * 2 + 1;
            boolean flagsub = false;
            for (int i = 1; i <= (k+1)*2-1; i++) {
                for (int j = 0; j <= strokeK; j++) {
                    if (j <= (strokeK-k1)/2 || j > strokeK-(strokeK-k1)/2) s += " ";
                    else {
                        s += "o ";
                    }
                }
                if (!flagsub) {
                    k1 += 1;
                    strokeK -= 1;
                }
                else {
                    k1 -= 1;
                    strokeK += 1;
                }
                if (k1 >= (k+1)*2-1) flagsub = true;
                if (i != (k+1)*2-1) s += "\n";
            }
        }
        return s;
    }
}
