package com.example.task3;

import java.util.HashMap;

public class Task {

    public static void main(String args[]) {
        System.out.println(solutions(1, 0, -1));
        System.out.println(solutions(1, 0, 0));
        System.out.println(solutions(1, 0, 1));
        System.out.println(findZip("all zip files are zipped"));
        System.out.println(findZip("all zip files are compressed"));
        System.out.println(checkPerfect(6));
        System.out.println(checkPerfect(28));
        System.out.println(checkPerfect(496));
        System.out.println(checkPerfect(12));
        System.out.println(checkPerfect(97));
        System.out.println(flipEndChars("Cat, dog, and mouse."));
        System.out.println(flipEndChars("ada"));
        System.out.println(flipEndChars("Ada"));
        System.out.println(flipEndChars("z"));
        System.out.println(isValidHexCode("#CD5C5C"));
        System.out.println(isValidHexCode("#EAECEE"));
        System.out.println(isValidHexCode("#eaecee"));
        System.out.println(isValidHexCode("#CD5C58C"));
        System.out.println(isValidHexCode("#CD5C5Z"));
        System.out.println(isValidHexCode("#CD5C&C"));
        System.out.println(isValidHexCode("CD5C5C"));
        int[] arr1 = {1, 3, 4, 4, 4};
        int[] arr2 = {2, 5, 7};
        int[] arr3 = {9, 8, 7, 6};
        int[] arr4 = {4, 4, 3, 1};
        int[] arr5 = {2};
        int[] arr6 = {3, 3, 3, 3, 3};
        System.out.println(same(arr1, arr2));
        System.out.println(same(arr3, arr4));
        System.out.println(same(arr5, arr6));
        System.out.println(isKaprekar(3));
        System.out.println(isKaprekar(5));
        System.out.println(isKaprekar(297));
        System.out.println(longestZero("01100001011000"));
        System.out.println(longestZero("100100100"));
        System.out.println(longestZero("11111"));
        System.out.println(nextPrime(12));
        System.out.println(nextPrime(24));
        System.out.println(nextPrime(11));
        System.out.println(rightTriangle(3, 4, 5));
        System.out.println(rightTriangle(145, 105, 100));
        System.out.println(rightTriangle(70, 130, 110));
    }

    private static int solutions(int a, int b, int c) {
        int d = b*b - 4*a*c;
        if (d > 0) return 2;
        if (d == 0) return 1;
        return 0;
    }

    private static int findZip(String a) {
        int counter = 0;
        int pos = 0;
        String b;
        for (int i = 2; i < a.length(); i++) {
            b = a.substring(i-2, i+1);
            if (b.equals("zip")){
                counter += 1;
                if (counter == 2) return i-2;
                pos = i-2;
            }
        }

        if (counter < 2) return -1;
        return pos;
    }

    private static boolean checkPerfect(int a) {
        int sum = 0;
        for (int i = 1; i <= a/2; i++) {
            if (a % i == 0) sum += i;
        }
        if (sum == a) return true;
        return false;
    }

    private static String flipEndChars(String a) {
        if (a.length() < 2) return "Incompatible.";
        if (a.charAt(0) == a.charAt(a.length()-1)) return "Two's a pair.";

        String str = "" + a.charAt(a.length()-1);
        for (int i = 1; i < a.length()-1; i++) str += a.charAt(i);
        return str + a.charAt(0);
    }

    private static boolean isValidHexCode(String a) {
        if (a.length() > 7) return false;
        return a.matches("#[0-9a-fA-F]+");
    }

    private static boolean same(int[] a, int[] b) {
        HashMap<Integer, Integer> aCounters = new HashMap<>();
        HashMap<Integer, Integer> bCounters = new HashMap<>();
        int aUniqueCounter = 0;
        int bUniqueCounter = 0;

        for (int i : a) {
            if (aCounters.containsKey(i)) {
                aCounters.computeIfPresent(i, (k, v) -> v + 1);
            }
            else {
                aCounters.put(i, 1);
                aUniqueCounter += 1;
            }
        }

        for (int i : b) {
            if (bCounters.containsKey(i)) {
                bCounters.computeIfPresent(i, (k, v) -> v + 1);
            }
            else {
                bCounters.put(i, 1);
                bUniqueCounter += 1;
            }
        }
        return aUniqueCounter == bUniqueCounter;
    }

    private static boolean isKaprekar(int a) {
        if (a == 0 || a == 1) return true;
        String n2 = String.valueOf(a*a);
        if (n2.length() == 1) n2 = "0" + n2;
        int len;
        if (n2.length() % 2 == 0) len = n2.length();
        else len = n2.length()-1;
        return String.valueOf(a).equals(String.valueOf(
                Integer.parseInt(n2.substring(0, len/2)) + Integer.parseInt(n2.substring(len/2))));
    }

    private static String longestZero(String a) {
        String[] zeros = a.split("1");
        int maxLen = 0;
        String maxZero = "";
        for (String zero : zeros) {
            if (zero.length() > maxLen) {
                maxLen = zero.length();
                maxZero = zero;
            }
        }
        return maxZero;
    }

    private static int nextPrime(int a) {
        for (int i = 2; i < a/2; i++) {
            if (a % i == 0) {
                int j = a + 1;
                while (!isPrime(j)) j += 1;
                return j;
            }
        }
        return a;
    }

    private static boolean isPrime(int a) {
        for (int i = 2; i < a/2; i++) {
            if (a % i == 0) return false;
        }
        return true;
    }

    private static boolean rightTriangle(int a, int b, int c) {
        if (a*a == b*b + c*c || b*b == a*a + c*c || c*c == a*a + b*b) return true;
        return false;
    }
}
