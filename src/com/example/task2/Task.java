package com.example.task2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Task {

    public static void main(String args[]) {
        System.out.println(repeat("mice", 5));
        System.out.println(repeat("hello", 3));
        System.out.println(repeat("stop", 1));
        int[] list1 = {10, 4, 1, 4, -10, -50, 32, 21};
        System.out.println(differenceMaxMin(list1));
        int[] list2 = {44, 32, 86, 19};
        System.out.println(differenceMaxMin(list2));
        int[] list3 = {1, 3};
        int[] list4 = {1, 2, 3, 4};
        int[] list5 = {1, 5, 6};
        int[] list6 = {1, 1, 1};
        int[] list7 = {9, 2, 2, 5};
        System.out.println(isAvgWhole(list3));
        System.out.println(isAvgWhole(list4));
        System.out.println(isAvgWhole(list5));
        System.out.println(isAvgWhole(list6));
        System.out.println(isAvgWhole(list7));
        int[] list8 = {1, 2, 3};
        int[] list9 = {1, -2, 3};
        int[] list10 = {3, 3, -2, 408, 3, 3};
        System.out.println(Arrays.toString(cumulativeSum(list8)));
        System.out.println(Arrays.toString(cumulativeSum(list9)));
        System.out.println(Arrays.toString(cumulativeSum(list10)));
        System.out.println(getDecimalPlaces("43.20"));
        System.out.println(getDecimalPlaces("400"));
        System.out.println(getDecimalPlaces("3.1"));
        System.out.println(Fibonacci(3));
        System.out.println(Fibonacci(7));
        System.out.println(Fibonacci(12));
        System.out.println(isValid("59001"));
        System.out.println(isValid("853a7"));
        System.out.println(isValid("732 32"));
        System.out.println(isValid("393939"));
        System.out.println(isStrangePair("ratio", "orator"));
        System.out.println(isStrangePair("sparkling", "groups"));
        System.out.println(isStrangePair("bush", "hubris"));
        System.out.println(isStrangePair("", ""));
        System.out.println(isPrefix("automation", "auto-"));
        System.out.println(isSuffix("arachnophobia", "-phobia"));
        System.out.println(isPrefix("retrospect", "sub-"));
        System.out.println(isSuffix("vocation", "-logy"));
        System.out.println(boxSeq(0));
        System.out.println(boxSeq(1));
        System.out.println(boxSeq(2));
    }

    private static String repeat(String a, int b) {
        String res = "";
        for(int i = 0; i<a.length(); i++) {
            for(int j = 0; j<b; j++) {
                res += a.charAt(i);
            }
        }
        return res;
    }

    private static int differenceMaxMin(int[] a) {
        return Arrays.stream(a).max().getAsInt()-Arrays.stream(a).min().getAsInt();
    }

    private static boolean isAvgWhole(int[] a) {
        if (Arrays.stream(a).average().getAsDouble() % 1 == 0) {
            return true;
        } else {
            return false;
        }
    }

    private static int[] cumulativeSum(int[] a) {
        int[] res = Arrays.copyOf(a, a.length);
        for (int i = 1; i<a.length; i++) {
            for (int j = 0; j<i; j++) {
                res[i] += a[j];
            }
        }
        return res;
    }

    private static int getDecimalPlaces(String a) {
        if (a.contains(".")) return a.length()-a.indexOf(".")-1;
        return 0;
    }

    private static int Fibonacci(int a) {
        int[] res = {1, 1, 0};
        if (a > 2) {
            for (int i = 1; i<a; i++) {
                res[2] = res[0] + res[1];
                res[0] = res[1];
                res[1] = res[2];
            }
        }
        return res[2];
    }

    private static boolean isValid(String a) {
        try {
            Integer.parseInt(a);
            if (a.length() == 5) {
                return true;
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isStrangePair(String a, String b) {
        if (Objects.equals(a, "") && Objects.equals(b, "")) return true;
        if (a.charAt(0) == b.charAt(b.length()-1) && a.charAt(a.length()-1) == b.charAt(0)) return true;
        return false;
    }

    private static boolean isPrefix(String a, String b) {
        boolean res = false;
        for (int i = 0; i < b.length()-1; i++) {
            if (a.charAt(i) == b.charAt(i)) {
                res = true;
            }
            else {
                return false;
            }
        }
        return res;
    }

    private static boolean isSuffix(String a, String b) {
        boolean res = false;
        for (int i = 1; i < b.length(); i++) {
            if (a.charAt(a.length()-b.length()+i) == b.charAt(i)) {
                res = true;
            }
            else {
                return false;
            }
        }
        return res;
    }

    private static int boxSeq(int a) {
        if (a == 0) return 0;
        if (a % 2 == 1) return (a/2)*2+3;
        if (a % 2 == 0) return (a/2)*2;
        return 0;
    }
}
