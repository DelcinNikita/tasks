package com.example.task1;


import java.util.ArrayList;
import java.util.Arrays;

public class Task {

    public static void main(String args[]) {
        System.out.println("remainder(1, 3) ➞ " + remainder(1, 3));
        System.out.println("remainder(3, 4) ➞ " + remainder(3, 4));
        System.out.println("remainder(-9, 45) ➞ " + remainder(-9, 45));
        System.out.println("remainder(5, 5) ➞ " + remainder(5, 5));
        System.out.println("triArea(3, 2) ➞ " + triArea(3, 2));
        System.out.println("triArea(7, 4) ➞ " + triArea(7, 4));
        System.out.println("triArea(10, 10) ➞ " + triArea(10, 10));
        System.out.println("animals(2, 3, 5) ➞ " + animals(2, 3, 5));
        System.out.println("animals(1, 2, 3) ➞ " + animals(1, 2, 3));
        System.out.println("animals(5, 2, 8) ➞ " + animals(5, 2, 8));
        System.out.println("profitableGamble(0.2, 50, 9) ➞ " + profitableGamble(0.2, 50, 9));
        System.out.println("profitableGamble(0.9, 1, 2) ➞ " + profitableGamble(0.9, 1, 2));
        System.out.println("profitableGamble(0.9, 3, 2) ➞ " + profitableGamble(0.9, 3, 2));
        System.out.println("operation(24, 15, 9) ➞ " + operation(24, 15, 9));
        System.out.println("operation(24, 26, 2) ➞ " + operation(24, 26, 2));
        System.out.println("operation(15, 11, 11) ➞ " + operation(15, 11, 11));
        System.out.println("ctoa('A') ➞ " + ctoa('A'));
        System.out.println("ctoa('m') ➞ " + ctoa('m'));
        System.out.println("ctoa('[') ➞ " + ctoa('['));
        System.out.println("ctoa('/') ➞ " + ctoa('/'));
        System.out.println("addUpTo(3) ➞ " + addUpTo(3));
        System.out.println("addUpTo(10) ➞ " + addUpTo(10));
        System.out.println("addUpTo(7) ➞ " + addUpTo(7));
        System.out.println("nextEdge(8, 10) ➞ " + nextEdge(8, 10));
        System.out.println("nextEdge(5, 7) ➞ " + nextEdge(5, 7));
        System.out.println("nextEdge(9, 2) ➞ " + nextEdge(9, 2));
        ArrayList<Integer> list1 = new ArrayList<Integer>(Arrays.asList(1, 5, 9));
        System.out.println("sumOfCubes([1, 5, 9]) ➞ " + sumOfCubes(list1));
        ArrayList<Integer> list2 = new ArrayList<Integer>(Arrays.asList(3, 4, 5));
        System.out.println("sumOfCubes([3, 4, 5]) ➞ " + sumOfCubes(list2));
        ArrayList<Integer> list3 = new ArrayList<Integer>(Arrays.asList(2));
        System.out.println("sumOfCubes([2]) ➞ " + sumOfCubes(list3));
        ArrayList<Integer> list4 = new ArrayList<Integer>();
        System.out.println("sumOfCubes([]) ➞ " + sumOfCubes(list4));
        System.out.println("abcmath(42, 5, 10) ➞ " + abcmath(42, 5, 10));
        System.out.println("abcmath(5, 2, 1) ➞ " + abcmath(5, 2, 1));
        System.out.println("abcmath(1, 2, 3) ➞ " + abcmath(1, 2, 3));

    }

    private static Integer remainder(Integer a, Integer b) {
        return a % b;
    }

    private static Integer triArea(Integer a, Integer b) {
        double res = 0.5*a*b;
        return (int) res;
    }

    private static Integer animals(Integer a, Integer b, Integer c) {
        return a*2+b*4+c*4;
    }

    private static Boolean profitableGamble(Double a, Integer b, Integer c) {
        if (a*b > c) {
            return true;
        } else {
            return false;
        }
    }

    private static String operation(Integer a, Integer b, Integer c) {
        if (a.equals(b+c)) {
            return "added";
        } else if (a.equals(b-c)) {
            return "subtracted";
        } else if (a.equals(b*c)) {
            return "multiplied";
        } else if (a.equals(b/c)) {
            return "divided";
        }
        return "none";
    }

    private static Integer ctoa(char a) {
        return (int) a;
    }

    private static Integer addUpTo(Integer a) {
        int sum = 0;
        for (int i = 1; i <= a; i++) {
            sum += i;
        }
        return sum;
    }

    private static Integer nextEdge(Integer a, Integer b) {
        return a+b-1;
    }

    private static Integer sumOfCubes(ArrayList<Integer> a) {
        int sum = 0;
        if (!a.isEmpty()) {
            for (int i = 0; i < a.size(); i++) {
                sum += a.get(i) * a.get(i) * a.get(i);
            }
        } else {
            return 0;
        }
        return sum;
    }

    private static Boolean abcmath(Integer a, Integer b, Integer c) {
        int sum = a;
        for (int i = 0; i < b; i++) {
            sum += sum;
        }
        if (sum % c == 0) {
            return true;
        }
        return false;
    }
}
