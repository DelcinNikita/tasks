package com.example.task6;

import java.util.*;

public class Task {

    public static void main(String[] args) {
        System.out.println(bell(1));
        System.out.println(bell(2));
        System.out.println(bell(3));
        System.out.println(translateWord("flag"));
        System.out.println(translateWord("Apple"));
        System.out.println(translateWord("button"));
        System.out.println(translateWord(""));
        System.out.println(translateSentence("I like to eat honey waffles."));
        System.out.println(translateSentence("Do you think it is going to rain today?"));
        System.out.println(validColor("rgb(0,0,0)"));
        System.out.println(validColor("rgb(0,,0)"));
        System.out.println(validColor("rgb(255,256,255)"));
        System.out.println(validColor("rgba(0,0,0,0.123456789)"));
        System.out.println(stripUrlParams("a=2"));
        System.out.println(stripUrlParams("https://edabit.com?a=1&b=2&a=2"));
        String[] arr1 = {"b"};
        System.out.println(stripUrlParams("https://edabit.com?a=1&b=2&a=2", arr1));
        System.out.println(stripUrlParams("https://edabit.com", arr1));
        System.out.println(getHashTags("How the Avocado Became the Fruit of the Global Trade"));
        System.out.println(getHashTags("Why You Will Probably Pay More for Your Christmas Tree This Year"));
        System.out.println(getHashTags("Hey Parents, Surprise, Fruit Juice Is Not Fruit"));
        System.out.println(getHashTags("Visualizing Science"));

    }

    private static int stirl(int a, int b) {
        if ( a == 0 && b == 0) {
            return 1;
        } else if ( b == 0) {
            return 0;
        } else if ( b > a) {
            return 0;
        } else {
            return stirl(a-1, b-1) + b*stirl(a-1, b);
        }
    }

    private static int bell(int a) {
        int ans = 0;
        for ( int i = 0; i <= a; i++) {
            ans += stirl(a, i);
        }
        return ans;
    }

    private static String translateWord(String a) {
        if (a.equals("")) return "";
        List<Character> vowels = Arrays.asList('A', 'E', 'I', 'O', 'U', 'Y', 'a', 'e', 'i', 'o', 'u', 'y');
        List<Character> consonants = Arrays.asList('B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
                'S', 'T', 'V', 'W', 'X', 'Z', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r',
                's', 't', 'v', 'w', 'x', 'z');
        List<String> dots = Arrays.asList(".", ",", ":", ";", "!", "?");

        if (vowels.contains(a.charAt(0))) {
            for (String dot : dots) {
                if (a.contains(dot)) {
                    String res = "";
                    for (int i = 0; i < a.length()-1; i++) {
                        res += a.charAt(i);
                    }
                    return res + "yay" + dot;
                }
            }
            return a + "yay";
        }
        if (consonants.contains(a.charAt(0))) {
            String res = "";
            for (String dot : dots) {
                if (a.contains(dot)) {
                    for (int i = 0; i < a.length()-1; i++) {
                        res += a.charAt(i);
                    }
                    boolean upper = false;
                    if (String.valueOf(a.charAt(0)).equals(String.valueOf(a.charAt(0)).toUpperCase())) {
                        upper = true;
                        res = res.toLowerCase();
                    }
                    res = res.substring(1);
                    int i = 0;
                    while (consonants.contains(a.charAt(i))) {
                        res += String.valueOf(a.charAt(i)).toLowerCase();
                        i += 1;
                        if (consonants.contains(a.charAt(i))) res = res.substring(1);
                    }
                    if (upper) return String.valueOf(res.charAt(0)).toUpperCase() + res.substring(1) + "ay" + dot;
                    else return res + "ay" + dot;

                }
            }
            res = a;
            boolean upper = false;
            if (String.valueOf(a.charAt(0)).equals(String.valueOf(a.charAt(0)).toUpperCase())) {
                upper = true;
                res = res.toLowerCase();
            }
            res = res.substring(1);
            int i = 0;
            while (consonants.contains(a.charAt(i))) {
                res += String.valueOf(a.charAt(i)).toLowerCase();
                i += 1;
                if (consonants.contains(a.charAt(i))) res = res.substring(1);
            }
            if (upper) return String.valueOf(res.charAt(0)).toUpperCase() + res.substring(1) + "ay";
            else return res + "ay";
        }
        return a;
    }

    private static String translateSentence(String a) {
        String res = "";
        String[] words = a.split(" ");
        for (String word : words) res += translateWord(word) + " ";
        return res;
    }

    private static boolean validColor(String a) {
        String b;
        boolean isRGBA;
        if (a.contains("rgba(")) {
            b = a.replace("rgba(", "");
            b = b.replace(")", "");
            isRGBA = true;
        }
        else if (a.contains("rgb(")) {
            b = a.replace("rgb(", "");
            b = b.replace(")", "");
            isRGBA = false;
        }
        else return false;

        List<String> values = List.of(b.split(","));
        if (isRGBA) {
            if (values.size() != 4 || values.contains("")) return false;
            for (int i = 0; i < values.size()-1; i++) if (Integer.parseInt(values.get(i)) > 255) return false;
            if (Double.parseDouble(values.get(3)) > 1) return false;
            return true;
        }
        else {
            if (values.size() != 3 || values.contains("")) return false;
            for (int i = 0; i < values.size()-1; i++) if (Integer.parseInt(values.get(i)) > 255) return false;
            return true;
        }
    }

    private static String stripUrlParams(String a, String ... b) {
        if (!a.contains("?")) return a;
        List<String> ban = Arrays.asList(b);
        String url = a.substring(0, a.indexOf("?") + 1);
        List<String> params = Arrays.asList(a.substring(a.indexOf("?") + 1).split("&"));
        List<String> newParams = new ArrayList<>();
        List<String> indexes = new ArrayList<>();
        for (String param : params) {
            String[] pair = param.split("=");
            if (!ban.contains(pair[0]))
                if (!indexes.contains(pair[0])) {
                    indexes.add(pair[0]);
                    newParams.add(param);
                }
                else newParams.set(indexes.indexOf(pair[0]), param);
        }
        for (String param : newParams) url += param;
        return url;
    }

    private static List<String> getHashTags(String a) {
        if (a.equals("")) return new ArrayList<>();
        String[] words = a.split(" ");
        List<String> ans = new ArrayList<>();
        ArrayList<Integer> lens = new ArrayList<>();
        if (words.length == 1) return List.of("#" + words[0]);
        ans.add(words[0].toLowerCase());
        lens.add(words[0].length());
        if (words.length == 2) return List.of("#" + words[0], "#" + words[1]);
        ans.add(words[1].toLowerCase());
        lens.add(words[1].length());
        if (words.length == 3) return List.of("#" + words[0], "#" + words[1], "#" + words[2]);
        ans.add(words[2].toLowerCase());
        lens.add(words[2].length());
        for (int i = 3; i < words.length; i++) {
            System.out.println(Collections.min(lens));
            System.out.println(ans.get(lens.indexOf(Collections.min(lens))));
            System.out.println(lens.indexOf(Collections.min(lens)));
            if (words[i].length() > Collections.min(lens)) {
                lens.set(lens.indexOf(Collections.min(lens)), words[i].length());
                ans.set(lens.indexOf(Collections.min(lens)), words[i].toLowerCase());
            }
        }
        return List.of("#" + ans.get(0), "#" + ans.get(1), "#" + ans.get(2));
    }
}
