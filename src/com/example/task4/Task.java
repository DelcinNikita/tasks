package com.example.task4;

import java.sql.Array;
import java.util.*;

public class Task {

    public static void main(String args[]) {
        essay(10,7, "hello my name is Bessie and this is my essay");
        System.out.println(split("()()()"));
        System.out.println(split("((()))"));
        System.out.println(split("((()))(())()()(()())"));
        System.out.println(split("((())())(()(()()))"));
        System.out.println(toCamelCase("hello_edabit"));
        System.out.println(toSnakeCase("helloEdabit"));
        System.out.println(toCamelCase("is_modal_open"));
        System.out.println(toSnakeCase("getColor"));
        double[] arr1 = {9, 17, 30, 1.5};
        double[] arr2 = {16, 18, 30, 1.8};
        double[] arr3 = {13.25, 15, 30, 1.5};
        System.out.println(overTime(arr1));
        System.out.println(overTime(arr2));
        System.out.println(overTime(arr3));
        System.out.println(BMI("205 pounds", "73 inches"));
        System.out.println(BMI("55 kilos", "1.65 meters"));
        System.out.println(BMI("154 pounds", "2 meters"));
        System.out.println(bugger(39));
        System.out.println(bugger(999));
        System.out.println(bugger(4));
        System.out.println(toStarShorthand("abbccc"));
        System.out.println(toStarShorthand("77777geff"));
        System.out.println(toStarShorthand("abc"));
        System.out.println(toStarShorthand(""));
        System.out.println(doesRhyme("Sam I am!", "Green eggs and ham."));
        System.out.println(doesRhyme("Sam I am!", "Green eggs and HAM."));
        System.out.println(doesRhyme("You are off to the races", "a splendid day."));
        System.out.println(doesRhyme("and frequently do?", "you gotta move."));
        System.out.println(trouble(451999277, 41177722899l));
        System.out.println(trouble(1222345, 12345));
        System.out.println(trouble(666789, 12345667));
        System.out.println(trouble(33789, 12345337));
        System.out.println(countUniqueBooks("AZYWABBCATTTA", 'A'));
        System.out.println(countUniqueBooks("$AA$BBCATT$C$$B$", '$'));
        System.out.println(countUniqueBooks("ZZABCDEF", 'Z'));


    }

    private static void essay(int a, int b, String c) {
        String[] words = c.split(" ");
        int wordCounter = 0;
        while (wordCounter < a) {
            String stroke = "";
            String rightStroke = "";
            int charCounter = 0;
            while (charCounter <= b) {
                if (wordCounter < a && (stroke + words[wordCounter]).length() <= b) {
                    stroke += words[wordCounter];
                    rightStroke += words[wordCounter] + " ";
                    charCounter = stroke.length();
                    wordCounter += 1;
                }
                else {
                    charCounter = b+1;
                    System.out.println(rightStroke);
                }
            }
        }
    }

    private static List<String> split(String a) {
        int k1 = 0;
        int k2 = 0;
        String str = "";
        List<String> strs = new ArrayList<String>();
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == '(') {
                k1 += 1;
                str += a.charAt(i);
            }
            else if (a.charAt(i) == ')') {
                k2 += 1;
                str += a.charAt(i);
            }
            else str += a.charAt(i);
            if (k1 == k2) {
                strs.add(str);
                str = "";
                k1 = 0;
                k2 = 0;
            }
        }
        return strs;
    }

    private static String toCamelCase(String a) {
        String[] s = a.split("_");
        String str = s[0];
        for (int i = 1; i < s.length; i++) {
            String ch = "" + s[i].charAt(0);
            str += ch.toUpperCase();
            for (int j = 1; j < s[i].length(); j++) {
                str += s[i].charAt(j);
            }
        }
        return str;
    }

    private static String toSnakeCase(String a) {
        String str = "";
        for (int i = 0; i < a.length(); i++) {
            String ch = "" + a.charAt(i);
            if (ch.toUpperCase().equals(ch)) str += "_" + ch.toLowerCase();
            else str += ch;
        }
        return str;
    }

    private static String overTime(double[] a) {
        double salary = 0;
        String strDouble = String.valueOf(a[0]);
        int hours = Integer.parseInt(strDouble.substring(0, strDouble.indexOf(".")));
        if (!strDouble.substring(strDouble.indexOf(".") + 1).equals("0")) {
            salary += ((Math.pow(10, strDouble.substring(strDouble.indexOf(".")+1).length()))-Integer.parseInt(strDouble.substring(strDouble.indexOf(".")+1)))*a[2]/
                    (Math.pow(10, strDouble.substring(strDouble.indexOf(".")+1).length()));
            hours += 1;
        }

        if (a[1] >= 17) {
            for (double i = hours; i < 17; i++) {
                salary += a[2];
            }
            for (double i = a[1]; i > 17; i--) salary += a[2] * a[3];
        }
        else {
            for (double i = hours; i < a[1]; i++) {
                salary += a[2];
            }
        }
        return "$"+ salary;
    }

    private static String BMI(String a, String b) {
        String[] as = a.split(" ");
        String[] bs = b.split(" ");
        double weight;
        double height;
        if (as[1].equals("pounds")) weight = Double.parseDouble(as[0])*0.4536;
        else weight = Double.parseDouble(as[0]);
        if (bs[1].equals("inches")) height = Double.parseDouble(bs[0])*0.0254;
        else height = Double.parseDouble(bs[0]);
        if (weight/(height*height) < 18.5) return (double)Math.round(weight/(height*height) * 10d) / 10d  + " Underweight";
        else if (18.5 <= weight/(height*height) && weight/(height*height) < 25) return (double)Math.round(weight/(height*height) * 10d) / 10d + " Normal weight";
        return (double)Math.round(weight/(height*height) * 10d) / 10d + " Underweight";
    }

    private static int bugger(int a) {
        String[] as = String.valueOf(a).split("");
        String k = String.valueOf(a);
        int counter = 0;
        while (k.length() > 1) {
            int mul = 1;
            for (String s : as) mul *= Integer.parseInt(s);
            counter += 1;
            k = String.valueOf(mul);
            as = k.split("");
        }
        return counter;
    }

    private static String toStarShorthand(String a) {
        if (a.equals("")) return "";
        Character prev = a.charAt(0);
        int counter = 1;
        String str = "";
        for (int i = 1; i < a.length(); i++){
            if (a.charAt(i) == prev) counter += 1;
            else {
                prev = a.charAt(i);
                if (counter != 1) str += a.charAt(i-1) + "*" + counter;
                else str += a.charAt(i-1);
                counter = 1;
            }
        }
        if (counter != 1) str += a.charAt(a.length()-1) + "*" + counter;
        else str += a.charAt(a.length()-1);
        return str;
    }

    private static  boolean doesRhyme(String a, String b) {
        List<Character> vokals = List.of('a', 'e', 'i', 'u', 'y', 'o');
        String[] as = a.split(" ");
        String[] bs = b.split(" ");
        String lasta = as[as.length-1].toLowerCase();
        String lastb = bs[bs.length-1].toLowerCase();
        HashMap<Character, Integer> aCounters = new HashMap<>();
        HashMap<Character, Integer> bCounters = new HashMap<>();
        for (int i = 0; i < lasta.length(); i++) {
            if (vokals.contains(lasta.charAt(i))) {
                if (aCounters.containsKey(lasta.charAt(i))) aCounters.computeIfPresent(lasta.charAt(i), (k, v) -> v + 1);
                else aCounters.put(lasta.charAt(i), 1);
            }
        }
        for (int i = 0; i < lastb.length(); i++) {
            if (vokals.contains(lastb.charAt(i))) {
                if (bCounters.containsKey(lastb.charAt(i))) bCounters.computeIfPresent(lastb.charAt(i), (k, v) -> v + 1);
                else bCounters.put(lastb.charAt(i), 1);
            }
        }
        return aCounters.equals(bCounters);
    }

    private static boolean trouble(long a, long b) {
        String as = String.valueOf(a);
        String bs = String.valueOf(b);
        for (int i = 2; i < as.length(); i++) {
            if (as.charAt(i) == as.charAt(i-1) && as.charAt(i)== as.charAt(i-2)){
                for (int j = 1; j < bs.length(); j++) {
                    if (as.charAt(i) == bs.charAt(j) && bs.charAt(j) == bs.charAt(j-1)) return true;
                }
            }
        }
        return false;
    }

    private static int countUniqueBooks(String a, Character b) {
        List<Character> list = new ArrayList<>();
        boolean c = false;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) != b && c && !list.contains(a.charAt(i))) list.add(a.charAt(i));
            if (a.charAt(i) == b && !c) c = true;
            else if (a.charAt(i) == b && c) c = false;
        }
        return list.size();
    }
}
